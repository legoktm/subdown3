## 1.0.2 / 2024-05-21
* Mark crate as abandoned

## 1.0.1 / 2021-11-21
* Update Tokio for RUSTSEC-2021-0124

## 1.0.0 / 2021-09-28
* Initial stable release
