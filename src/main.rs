/*
Downloads images from a subreddit
Copyright (C) 2020 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use anyhow::Result;
use lazy_static::lazy_static;
use regex::Regex;
use reqwest::blocking::Client;
use reqwest::header;
use serde::Deserialize;
use std::collections::BTreeMap;
use std::env;
use std::fs;
use std::iter::Iterator;
use std::path::Path;
use thiserror::Error;

const VERSION: &str = env!("CARGO_PKG_VERSION");
/// Client-ID for imgur
const IMGUR_CLIENT_ID: &str = "Client-ID 285892c707b75ab";

#[derive(Error, Debug)]
enum SubdownError {
    #[error("Failed to fetch {url}: status {status}")]
    FetchFailure { status: String, url: String },
    #[error("{0}")]
    InvalidArg(String),
}

#[derive(Deserialize, Clone)]
struct RedditPost {
    id: String,
    title: String,
    url: String,
    is_self: bool,
    is_video: bool,
    #[serde(default)]
    is_gallery: bool,
    media_metadata: Option<RedditGallery>,
    #[serde(default)]
    crosspost_parent_list: Vec<RedditPost>,
    crosspost_parent: Option<String>,
    domain: String,
    // unused:
    // subreddit: String,
}

type RedditGallery = BTreeMap<String, GalleryItem>;

#[derive(Deserialize, Clone)]
struct GalleryItem {
    m: String,
}

#[derive(Deserialize)]
struct RedditChild {
    data: RedditPost,
}

#[derive(Deserialize)]
struct RedditData {
    children: Vec<RedditChild>,
    after: Option<String>,
}

#[derive(Deserialize)]
struct RedditListing {
    data: RedditData,
}

#[derive(Deserialize)]
struct GfyCat {
    #[serde(rename = "gfyItem")]
    gfy_item: GfyItem,
}

#[derive(Deserialize)]
struct GfyItem {
    #[serde(rename = "mp4Url")]
    mp4_url: String,
}

#[derive(Deserialize)]
struct ImgurAlbum {
    data: Vec<ImgurImageData>,
}

#[derive(Deserialize)]
struct ImgurImage {
    data: ImgurImageData,
}

#[derive(Deserialize)]
struct ImgurImageData {
    link: String,
}

enum SpecialOption {
    Help,
    Version,
}

struct Options {
    subreddits: Vec<String>,
    limit: usize,
    top: Option<String>,
    special: Option<SpecialOption>,
}

#[derive(Clone)]
struct Query {
    after: Option<String>,
    top: Option<String>,
}

/// Shortcut whether a file/dir exists
fn exists(path: &str) -> bool {
    fs::metadata(&path).is_ok()
}

/// Shortcut to get a file/URL's extension
fn extension(path: &str) -> &str {
    Path::new(path).extension().unwrap().to_str().unwrap()
}

/// Saves the specified URL to the provided filename
fn download(client: &Client, url: String, fname: String) -> Result<()> {
    let mut dest = fs::File::create(fname)?;
    let mut resp = client.get(url.as_str()).send()?;
    if resp.status() == 403 || resp.status() == 404 {
        // Deleted file, not much we can do
        format!("{}: {}", resp.status(), resp.url());
        return Ok(());
    }
    if !resp.status().is_success() {
        return Err(anyhow::Error::from(SubdownError::FetchFailure {
            url: resp.url().to_string(),
            status: resp.status().to_string(),
        }));
    }
    // TODO: set mtime to post time
    resp.copy_to(&mut dest)?;
    Ok(())
}

/// Grabs URLs via imgur API
fn imgur(client: &Client, url: &str) -> Result<Vec<String>> {
    let mut urls: Vec<String> = vec![];
    lazy_static! {
        static ref RE_STRIP: Regex =
            Regex::new(r"^http(s)?://imgur.com/((a|gallery)/)?").unwrap();
        static ref RE_STRIP_ANCHOR: Regex = Regex::new(r"#(.*?)$").unwrap();
        // XXX: is there a better way than hardcoding imgur's filetypes?
        static ref RE_ID: Regex = Regex::new(r"([A-z0-9]*+)(/|#|,|$|\.(gifv?|png|jpg|mp4))").unwrap();
        static ref RE_ALBUM: Regex = Regex::new(r"/(gallery|a)/").unwrap();
    }
    let url_path = RE_STRIP_ANCHOR
        .replace(&RE_STRIP.replace(url, "").into_owned(), "")
        .into_owned();
    if !RE_ID.is_match(&url_path) {
        // Didn't extract any IDs
        panic!("Unsupported URL, please report this as a bug: {}", url);
    }
    let is_album = RE_ALBUM.is_match(url);
    for cap in RE_ID.captures_iter(&url_path) {
        let id = &cap[1];
        if is_album {
            let api_url =
                format!("https://api.imgur.com/3/album/{}/images", id);
            let resp = client
                .get(api_url.as_str())
                .header(header::AUTHORIZATION, IMGUR_CLIENT_ID)
                .send()?;
            if resp.status() == 404 {
                println!("404: {}", api_url);
                continue;
            }
            println!("Fetching {}", resp.url());
            if !resp.status().is_success() {
                return Err(anyhow::Error::from(SubdownError::FetchFailure {
                    url: resp.url().to_string(),
                    status: resp.status().to_string(),
                }));
            }
            //let debug: serde_json::Value = resp.json().unwrap();
            //dbg!(debug);
            let album: ImgurAlbum = resp.json()?;
            for image in album.data {
                urls.push(image.link.to_string());
            }
        } else {
            let api_url = format!("https://api.imgur.com/3/image/{}", id);
            let resp = client
                .get(api_url.as_str())
                .header(header::AUTHORIZATION, IMGUR_CLIENT_ID)
                .send()?;
            if resp.status() == 404 {
                println!("404: {}", api_url);
                continue;
            }
            if !resp.status().is_success() {
                return Err(anyhow::Error::from(SubdownError::FetchFailure {
                    url: resp.url().to_string(),
                    status: resp.status().to_string(),
                }));
            }
            println!("Fetched {}", resp.url());
            let image: ImgurImage = resp.json()?;
            urls.push(image.data.link);
        }
    }
    Ok(urls)
}

/// Normalizes i.imgur.com URLs
fn iimgur(url: &str) -> Vec<String> {
    // We need to specially handle gifv links
    // see http://fileformats.archiveteam.org/wiki/GIFV
    if extension(url) == "gifv" {
        // FIXME: does imgur no longer allow downloading gifs?
        vec![url.replace(".gifv", ".mp4")]
    } else {
        vec![url.to_string()]
    }
}

/// Grabs URLs via gfycat API
fn gfycat(client: &Client, url: &str) -> Result<Vec<String>> {
    let mut id = Path::new(&url).file_name().unwrap().to_str().unwrap();
    if id.contains('-') {
        // ID's like foo-bar-baz should just be foo
        let split: Vec<&str> = id.split('-').collect();
        id = split[0];
    }
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(gfycat|redgifs)\.com").unwrap();
    }
    let caps = RE.captures(url).unwrap();
    let kind = &caps[1];
    // We need to normalize the case of the ID so use the API
    let api_url = format!("https://api.{}.com/v1/gfycats/{}", kind, id);
    let resp = client.get(api_url.as_str()).send()?;
    println!("Fetching {}", resp.url());
    if resp.status() == 404 {
        return if kind == "gfycat" {
            // Special case, some gfycats were moved to redgifs
            gfycat(client, &RE.replace(url, "redgifs.com").into_owned())
        } else {
            println!("404: {}", resp.url());
            Ok(vec![])
        };
    }
    if !resp.status().is_success() {
        return Err(anyhow::Error::from(SubdownError::FetchFailure {
            url: resp.url().to_string(),
            status: resp.status().to_string(),
        }));
    }
    //let debug: serde_json::Value = resp.json().unwrap();
    //dbg!(debug);
    let gfy: GfyCat = resp.json()?;
    Ok(vec![gfy.gfy_item.mp4_url])
}

/// Rewrite thumbs.gfycat.com URLs
fn thumbs_gfycat(url: &str) -> Vec<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"gfycat.com/([A-z0-9]*?)-").unwrap();
    }
    if !RE.is_match(url) {
        panic!(
            "Unrecognized thumbs.gfycat.com URL: {}, please report a bug",
            url
        );
    }
    let caps = RE.captures(url).unwrap();
    vec![format!("https://giant.gfycat.com/{}.mp4", &caps[1])]
}

/// Rewrite media.giphy.com URLs
fn giphy(url: &str) -> Vec<String> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"/media/([A-z0-9]*?)/(source|giphy).gif$").unwrap();
    }
    if !RE.is_match(url) {
        panic!(
            "Unrecognized media.gifphy.com URL: {}, please report a bug",
            url
        );
    }
    let caps = RE.captures(url).unwrap();
    vec![format!("https://i.giphy.com/media/{}/source.gif", &caps[1])]
}

fn reddit_gallery(items: &RedditGallery) -> Vec<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^image/(.*?)$").unwrap();
    }
    items
        .iter()
        .filter_map(|(id, item)| {
            if !RE.is_match(&item.m) {
                // FIXME: Error output
                return None;
            }
            let extension = RE.captures(&item.m).unwrap();
            Some(format!("https://i.redd.it/{}.{}", &id, &extension[1]))
        })
        .collect()
}

struct Subreddit {
    name: String,
    posts: Vec<RedditPost>,
    query: Query,
    done: bool,
    client: Client,
}

impl Subreddit {
    fn new(name: &str, top: Option<String>) -> Subreddit {
        Subreddit {
            name: name.to_string(),
            posts: vec![],
            query: Query { after: None, top },
            done: false,
            client: client(),
        }
    }
}

impl Iterator for Subreddit {
    type Item = RedditPost;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }
        if !self.posts.is_empty() {
            return self.posts.pop();
        }
        let url = match &self.query.top {
            Some(_) => format!(
                "https://old.reddit.com/r/{}/top.json?{}",
                self.name,
                build_query(self.query.clone())
            ),
            None => format!(
                "https://old.reddit.com/r/{}.json?{}",
                self.name,
                build_query(self.query.clone())
            ),
        };
        let resp = self.client.get(url.as_str()).send().unwrap();
        if !resp.status().is_success() {
            panic!("Request failed");
        }
        if resp.url().as_str().contains("search.json") {
            panic!("Nonexistent subreddit specified");
        }
        println!("Fetched {}", resp.url());
        let listing: RedditListing = resp.json().unwrap();
        for child in listing.data.children {
            if child.data.crosspost_parent.is_some() {
                self.posts.push(child.data.crosspost_parent_list[0].clone());
            } else {
                self.posts.push(child.data);
            };
        }
        match listing.data.after {
            Some(after) => {
                self.query.after = Some(after);
            }
            None => {
                // Not enough posts to make another request anyways
                self.done = true;
            }
        }

        self.posts.pop()
    }
}

/// Helper to build the subreddit query string
fn build_query(query: Query) -> String {
    let built = match query.after {
        Some(after) => format!("after={}", after),
        None => "".to_string(),
    };
    match query.top {
        Some(top) => format!("{}&sort=top&t={}", built, top),
        None => built,
    }
}

/// Make sure post title can be used as a filename
fn normalize_title(original: &str) -> String {
    let mut title = original.replace("/", "_");
    // Safely under 255 (including post ID, extension, etc.)
    title.truncate(240);
    title
}

/// Process a single reddit post
fn handle_post(
    client: &Client,
    subreddit: &str,
    post: RedditPost,
) -> Result<()> {
    if post.is_self || post.is_video {
        return Ok(());
    }
    // Note, we need to use subreddit instead of post.subreddit so
    // we don't pick up the original subreddit of a crosspost
    if !exists(subreddit) {
        println!("Creating {} directory...", &subreddit);
        fs::create_dir(&subreddit)?;
    }

    let url = post.url.to_string();
    // println!("url: {}", post.url);
    let real_urls: Vec<String> = if post.is_gallery {
        reddit_gallery(&post.media_metadata.unwrap())
    } else {
        match post.domain.as_str() {
            "i.redd.it" => vec![url],
            "upload.wikimedia.org" => vec![url],
            "zippy.gfycat.com" => vec![url],
            "i.imgur.com" => iimgur(&post.url),
            "m.imgur.com" => iimgur(&post.url),
            "gfycat.com" => gfycat(client, &post.url)?,
            "thumbs.gfycat.com" => thumbs_gfycat(&post.url),
            "imgur.com" => imgur(client, &post.url)?,
            "media.giphy.com" => giphy(&post.url),
            // deviantart
            // tumblr
            _ => {
                println!("Skipping {}, unsupported domain", post.url);
                return Ok(());
            }
        }
    };
    let needs_counter = real_urls.len() > 1;
    for (counter, real_url) in real_urls.into_iter().enumerate() {
        let ext = extension(&real_url);
        if needs_counter {
            print!("Saving {} ({})...", post.url, counter + 1);
        } else {
            print!("Saving {}...", post.url);
        }
        let title = normalize_title(&post.title);
        let fname = if needs_counter {
            format!(
                "{}/{} ({}) - {}.{}",
                &subreddit,
                title,
                counter + 1,
                post.id,
                ext
            )
        } else {
            format!("{}/{} - {}.{}", &subreddit, title, post.id, ext)
        };
        if exists(fname.as_str()) {
            println!(" already exists.");
            continue;
        }
        download(client, real_url.to_string(), fname)?;
        println!(" done.");
    }

    Ok(())
}

/// Process CLI arguments
fn handle_args(original_args: Vec<String>) -> Result<Options> {
    let mut options = Options {
        subreddits: vec![],
        limit: 25,
        top: None,
        special: None,
    };
    let mut args = original_args;
    // Drop first element (binary name)
    args.remove(0);
    for arg in args {
        if !arg.starts_with("--") {
            // subreddit name
            options.subreddits.push(arg);
            continue;
        }
        if arg == "--help" {
            options.special = Some(SpecialOption::Help);
            return Ok(options);
        } else if arg == "--version" {
            options.special = Some(SpecialOption::Version);
            return Ok(options);
        }
        if !arg.contains('=') {
            return Err(anyhow::Error::from(SubdownError::InvalidArg(
                format!("Option {} is missing a value", arg),
            )));
        }
        let split: Vec<&str> = arg.split('=').collect();
        let val = split[1];
        match split[0] {
            "--limit" => {
                options.limit =
                    val.parse().expect("Invalid value specified for --limit");
            }
            "--top" => {
                let accepted = vec!["hour", "week", "month", "year", "all"];
                if !accepted.contains(&val) {
                    return Err(anyhow::Error::from(SubdownError::InvalidArg(
                        format!(
                            "Invalid value for --top. Accepted values are: {}",
                            accepted.join(", ")
                        ),
                    )));
                }
                options.top = Some(val.to_string());
            }
            _ => {
                return Err(anyhow::Error::from(SubdownError::InvalidArg(
                    format!("Unknown option \"{}\"", split[0]),
                )));
            }
        }
    }

    if options.subreddits.is_empty() {
        return Err(anyhow::Error::from(SubdownError::InvalidArg(
            "You need to provide a subreddit to download".to_string(),
        )));
    }

    Ok(options)
}

fn help() -> String {
    "\
usage: subdown3 [--help] [--limit=LIMIT] [--top=hour,week,month,year,all]
                [--version] [subreddits]
 positional arguments:
   subreddits                        list of subreddits to download

 optional arguments:
   --help                            show this help message and exit
   --limit=LIMIT                     posts to process
   --top=hour,week,month,year,all    get posts from the top listing
   --version                         display version
"
    .to_string()
}

/// Always set a User-Agent
fn client() -> Client {
    Client::builder()
        .user_agent(format!("subdown3/{}", VERSION))
        .build()
        .unwrap()
}

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let options = handle_args(args)?;
    match options.special {
        Some(SpecialOption::Help) => {
            println!("{}", help());
            return Ok(());
        }
        Some(SpecialOption::Version) => {
            println!("subdown3 {}", VERSION);
            return Ok(());
        }
        _ => {}
    }
    let client = client();
    for subreddit in options.subreddits {
        for (counter, post) in
            Subreddit::new(&subreddit, options.top.clone()).enumerate()
        {
            if counter > options.limit {
                break;
            }
            handle_post(&client, &subreddit, post)?;
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extension() {
        assert_eq!("jpg", extension("foo.jpg"));
        assert_eq!("png", extension("https://example.org/image.png"));
    }

    #[test]
    fn test_exists() {
        let path = "thisdoesntexistyet.test";
        assert_eq!(false, exists(path));
        fs::File::create(path).unwrap();
        assert_eq!(true, exists(path));
        fs::remove_file(path).unwrap();
    }

    #[test]
    fn test_gfycat() {
        let client = Client::new();
        assert_eq!(
            vec!["https://giant.gfycat.com/GrouchyAgonizingCrocodile.mp4"],
            gfycat(&client, "https://gfycat.com/grouchyagonizingcrocodile")
                .unwrap()
        );
        assert_eq!(
            vec!["https://giant.gfycat.com/GrouchyAgonizingCrocodile.mp4"],
            gfycat(
                &client,
                "https://gfycat.com/grouchyagonizingcrocodile-taylor-swift"
            )
            .unwrap()
        );
    }

    #[test]
    fn test_thumbs_gfycat() {
        assert_eq!(
            vec!["https://giant.gfycat.com/DecentDentalGonolek.mp4"],
            thumbs_gfycat("https://thumbs.gfycat.com/DecentDentalGonolek-size_restricted.gif")
        );
    }

    #[test]
    fn test_iimgur() {
        assert_eq!(
            vec!["https://i.imgur.com/0jeDsVV.jpg"],
            iimgur("https://i.imgur.com/0jeDsVV.jpg")
        );
        assert_eq!(
            vec!["https://i.imgur.com/0jeDsVV.mp4"],
            iimgur("https://i.imgur.com/0jeDsVV.gifv")
        );
    }

    #[test]
    fn test_imgur() {
        let client = client();
        // Album
        assert_eq!(
            vec!["https://i.imgur.com/2lh6gS7.jpg"],
            imgur(&client, "https://imgur.com/a/v0A2eGF").unwrap()
        );
        // Album with anchor
        assert_eq!(
            vec!["https://i.imgur.com/2lh6gS7.jpg"],
            imgur(&client, "https://imgur.com/a/v0A2eGF#0").unwrap()
        );
        // Individual photo
        assert_eq!(
            vec!["https://i.imgur.com/vPARnlm.jpg"],
            imgur(&client, "http://imgur.com/vPARnlm").unwrap()
        );
        // Comma separated ID list
        assert_eq!(
            vec![
                "https://i.imgur.com/D5Lvv.jpg",
                "https://i.imgur.com/ee15t.jpg",
                "https://i.imgur.com/A26Dv.jpg",
                "https://i.imgur.com/wXkVh.jpg"
            ],
            imgur(&client, "https://imgur.com/D5Lvv,ee15t,A26Dv,wXkVh")
                .unwrap()
        );
        // Gracefully handle 404s/deleted images
        let empty: Vec<String> = vec![];
        assert_eq!(
            empty,
            imgur(&client, "https://imgur.com/thisiddoesntexist").unwrap()
        );
    }

    #[test]
    fn test_giphy() {
        assert_eq!(
            vec!["https://i.giphy.com/media/9RfDOnyUdMWw8/source.gif"],
            giphy("https://media.giphy.com/media/9RfDOnyUdMWw8/giphy.gif")
        );
        assert_eq!(
            vec!["https://i.giphy.com/media/9RfDOnyUdMWw8/source.gif"],
            giphy("https://i.giphy.com/media/9RfDOnyUdMWw8/source.gif")
        );
    }

    #[test]
    fn test_reddit_gallery() {
        let gallery: RedditGallery = serde_json::from_str(
            r#"
            {
              "ikppjh0agzg51": {
                "m": "image/jpg",
                "id": "ikppjh0agzg51"
              },
              "buw8s9tvgzg51": {
                "m": "image/jpg",
                "id": "buw8s9tvgzg51"
              },
              "c9bynce2hzg51": {
                "m": "image/jpg",
                "id": "c9bynce2hzg51"
              }
            }
        "#,
        )
        .unwrap();
        assert_eq!(
            reddit_gallery(&gallery),
            vec![
                "https://i.redd.it/buw8s9tvgzg51.jpg",
                "https://i.redd.it/c9bynce2hzg51.jpg",
                "https://i.redd.it/ikppjh0agzg51.jpg"
            ]
        );
    }

    #[test]
    fn test_fetch_subreddit() {
        let mut subreddit = Subreddit::new("BendingWallpapers", None);
        match subreddit.next() {
            Some(_) => {}
            None => panic!("Unable to fetch posts"),
        }
    }

    #[test]
    #[should_panic(expected = "Nonexistent subreddit specified")]
    fn test_subreddit_nonexistent() {
        for _ in Subreddit::new("kasdjgfvjsdhfgsdjhfg", None) {}
    }

    #[test]
    fn test_handle_args() {
        let options = handle_args(vec![
            "subdown3".to_string(),
            "one".to_string(),
            "two".to_string(),
            "--limit=30".to_string(),
        ])
        .unwrap();
        assert_eq!(vec!["one", "two"], options.subreddits);
        assert_eq!(30, options.limit);
        assert_eq!(None, options.top);
        match handle_args(vec![
            "subdown3".to_string(),
            "one".to_string(),
            "--help".to_string(),
        ])
        .unwrap()
        .special
        {
            Some(SpecialOption::Help) => {}
            _ => panic!("Didn't parse --help properly"),
        }
    }

    #[test]
    fn test_handle_args_top() {
        assert_eq!(
            Some("all".to_string()),
            handle_args(vec![
                "subdown3".to_string(),
                "--top=all".to_string(),
                "foo".to_string()
            ])
            .unwrap()
            .top
        );
    }

    #[test]
    #[should_panic(
        expected = "Invalid value for --top. Accepted values are: hour, week, month, year, all"
    )]
    fn test_handle_args_invalid_top() {
        handle_args(vec![
            "subdown3".to_string(),
            "--top=wrong".to_string(),
            "foo".to_string(),
        ])
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "You need to provide a subreddit to download")]
    fn test_handle_args_no_subreddit() {
        handle_args(vec!["subdown3".to_string()]).unwrap();
    }

    #[test]
    #[should_panic(expected = "Unknown option \"--foo\"")]
    fn test_handle_args_unknown_option() {
        handle_args(vec![
            "subdown3".to_string(),
            "--foo=baz".to_string(),
            "one".to_string(),
        ])
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "Option --foo is missing a value")]
    fn test_handle_args_no_value() {
        handle_args(vec![
            "subdown3".to_string(),
            "--foo".to_string(),
            "one".to_string(),
        ])
        .unwrap();
    }

    #[test]
    fn test_normalize_title() {
        assert_eq!("foo_bar_baz", normalize_title("foo/bar/baz"));
        assert_eq!("a".repeat(240), normalize_title(&"a".repeat(500)));
    }

    #[test]
    fn test_build_query() {
        assert_eq!(
            "",
            build_query(Query {
                after: None,
                top: None
            })
        );
        assert_eq!(
            "after=123",
            build_query(Query {
                after: Some("123".to_string()),
                top: None
            })
        );
        assert_eq!(
            "&sort=top&t=all",
            build_query(Query {
                after: None,
                top: Some("all".to_string())
            })
        );
        assert_eq!(
            "after=123&sort=top&t=all",
            build_query(Query {
                after: Some("123".to_string()),
                top: Some("all".to_string())
            })
        );
    }
}
