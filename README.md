subdown3
========
[![crates.io](https://img.shields.io/crates/v/subdown3.svg)](https://crates.io/crates/subdown3)
[![pipeline status](https://gitlab.com/legoktm/subdown3/badges/master/pipeline.svg)](https://gitlab.com/legoktm/subdown3/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/subdown3/badges/master/coverage.svg)](https://legoktm.gitlab.io/subdown3/coverage/)

> Note: This project is no longer maintained. Feel free to fork if desired.

Download pictures and gifs from a subreddit. Great for subreddits full of
wallpapers or other cool images.

Install with: `cargo install subdown3`

Linux binaries can be [downloaded from GitLab](https://gitlab.com/legoktm/subdown3/-/releases).

Example: `subdown3 BendingWallpapers --top=all`

Full options:
```
usage: subdown3 [--help] [--limit=LIMIT] [--top=hour,week,month,year,all]
                [--version] [subreddits]
 positional arguments:
   subreddits                        list of subreddits to download

 optional arguments:
   --help                            show this help message and exit
   --limit=LIMIT                     posts to process
   --top=hour,week,month,year,all    get posts from the top listing
   --version                         display version
```
## Supported sites
subdown3 supports downloading from the following sites:
* i.redd.it 
* gfycat.com
* imgur.com
* giphy.com
* upload.wikimedia.org 

More can be added upon request.

## License
subdown3 is (C) 2020 Kunal Mehta and released under the GPL, v3 or any later
version. See COPYING for more details.
